-- 秒杀执行存储过程
DELIMITER $$ -- console ; 转为 $$
-- 定义存储过程(varchar类型参数需要声明长度，否则报错）
CREATE  PROCEDURE `seckill`.`execute_seckill`
  (IN v_seckill_id BIGINT,IN v_phone VARCHAR(20) ,IN v_kill_time TIMESTAMP ,
    OUT r_result int)
  BEGIN
    DECLARE insert_count int DEFAULT 0;
    START TRANSACTION ;
    INSERT ignore into success_killed
      (seckill_id,user_phone,create_time)
      VALUES (v_seckill_id,v_phone,v_kill_time);
    SELECT ROW_COUNT() INTO insert_count;
    IF(insert_count = 0) THEN
      ROLLBACK ;
      set r_result = -1;
    ELSEIF(insert_count <0 ) THEN
      ROLLBACK ;
      set r_result = -2;
    ELSE
      UPDATE seckill
      SET number = number-1
      WHERE seckill_id = v_seckill_id
        AND end_time > v_kill_time
        AND start_time < v_kill_time
        AND number > 0;
      SELECT ROW_COUNT() INTO insert_count;
      IF(insert_count = 0) THEN
        ROLLBACK ;
        set r_result = 0;
      ELSEIF(insert_count <0 ) THEN
        ROLLBACK ;
        set r_result = -2;
      ELSE
        COMMIT ;
        set r_result = 1;
      END IF;
    END IF;
  END ;
$$
-- 存储过程定义结束
DELIMITER ;
set @r_result = -3;
call execute_seckill(1003,'15366623546',now(),@r_result);

-- 获取结果
select @r_result

-- 存储过程
-- 1：存储过程优化：事务行级锁持有的时间
-- 2：不要过度依赖存储过程
-- 3：简单的逻辑可以应用存储过程
-- 4：QPS:一个秒杀单6000/qps