package wang.kingmo.exception;

/**
 * 重复秒杀异常（运行期异常）
 *
 * @author KingMo 2017/6/15
 */
public class RepeatKillException extends SeckillException {
    public RepeatKillException(String message) {
        super(message);
    }

    public RepeatKillException(String message, Throwable cause) {
        super(message, cause);
    }
}
