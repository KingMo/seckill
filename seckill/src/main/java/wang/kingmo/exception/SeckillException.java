package wang.kingmo.exception;

/**
 * 秒杀相关业务异常
 * @author KingMo 2017/6/15
 */
public class SeckillException extends RuntimeException {
    public SeckillException(String message) {
        super(message);
    }

    public SeckillException(String message, Throwable cause) {
        super(message, cause);
    }
}
