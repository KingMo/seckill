package wang.kingmo.exception;

/**
 * 秒杀关闭异常
 *
 * @author KingMo 2017/6/15
 */
public class SeckillCloseException extends SeckillException {

    public SeckillCloseException(String message) {
        super(message);
    }

    public SeckillCloseException(String message, Throwable cause) {
        super(message, cause);
    }
}
