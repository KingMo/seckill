package wang.kingmo.service;

import wang.kingmo.dto.Exposer;
import wang.kingmo.dto.SeckillExecution;
import wang.kingmo.entity.Seckill;
import wang.kingmo.exception.RepeatKillException;
import wang.kingmo.exception.SeckillCloseException;

import java.util.List;

/**
 * 业务接口：站在“使用者”角度设计接口
 * 三个方面：方法定义粒度，参数，返回类型（return 类型/异常）
 * @author KingMo 2017/6/15
 */
public interface SeckillService {

    /**
     * 查询所有秒杀记录
     * @return
     */
    List<Seckill> getSeckillList();

    /**
     * 查询单个秒杀记录
     * @param seckillId
     * @return
     */
    Seckill getById(long seckillId);

    /**
     * 秒杀开启时输出秒杀接口地址
     * 否则输出系统时间和秒杀时间
     * @param seckillId
     */
    Exposer exportSeckillUrl(long seckillId);


    /**
     * 执行秒杀
     * @param seckillId
     * @param userPhone
     * @return
     */
    SeckillExecution executeSeckill(long seckillId, String userPhone ,String md5)
        throws SeckillExecution,RepeatKillException,SeckillCloseException;

    /**
     * 执行秒杀by 存储过程
     * @param seckillId
     * @param userPhone
     * @return
     */
    SeckillExecution executeSeckillProcedure(long seckillId, String userPhone ,String md5);

}
