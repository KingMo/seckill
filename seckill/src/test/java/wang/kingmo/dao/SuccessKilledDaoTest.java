package wang.kingmo.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import wang.kingmo.entity.Seckill;
import wang.kingmo.entity.SuccessKilled;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器
 * spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
//告诉junit spring配置文件
@ContextConfiguration({"classpath:spring/spring-dao.xml"})
public class SuccessKilledDaoTest {

    //注入Dao实现类依赖
    @Resource
    SuccessKilledDao successKilledDao;

    @Test
    public void testInsertSuccessKilled() throws Exception {
        int status = successKilledDao.insertSuccessKilled(1001L, "15666666666");
        System.out.println(status);
    }

    @Test
    public void testQueryByIdWithSeckill() throws Exception {

        SuccessKilled successKilled = successKilledDao.queryByIdWithSeckill(1001L, "15666666666");

        if (null == successKilled) {
            System.out.println("无符合条件的数据");
        }else {
            Seckill seckill = successKilled.getSeckill();
            System.out.println("SuccessKilled:" + successKilled);
            System.out.println("seckill:" + seckill);
        }
    }

}